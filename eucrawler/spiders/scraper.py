import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule


class ScraperSpider(CrawlSpider):
    name = 'scraper'
    allowed_domains = ['europarl.europa.eu']
    start_urls = ['https://www.europarl.europa.eu/meps/de/full-list/all']

    rules = (
        Rule(LinkExtractor(allow=(r"meps/de/[1-9]*/.*/home",r"meps/de/[1-9]*\w$",r"meps/de/[1-9]*/\w$")), callback='parse_item', follow=True),
    )
    
    def parse_item(self, response):
        item = {}
        item['name'] = str(response.xpath('/html/body/main/section[1]/div/div[2]/div/div/div[1]/text()').get()).replace("\t","").replace("\r","").replace("\n","")
        item['fraktion'] = str(response.xpath('/html/body/main/section[1]/div/div[2]/div/div/h3/text()').get()).replace("\t","").replace("\r","").replace("\n","")
        parteiland=str(response.xpath("/html/body/main/section[1]/div/div[2]/div/div/div[3]/text()").get()).replace("\t","").replace("\r","").replace("\n","")
        item["land"]=parteiland[:parteiland.find("-")-2]
        item['partei'] =parteiland[parteiland.find("-")+2:]
        social= response.xpath("//div[@class='col-12']/div/div[2]/div")
        for i in social:
            email_counter=0
            website_counter=0
            instagram_counter=0
            facebook_counter=0
            twitch_counter=0
            twitter_counter=0

            for email  in i.xpath("a[@data-original-title='E-Mail']/@href").getall():
                item["email"+str(email_counter)]=str(email).replace("[dot]",".").replace("[at]","@")[7:][::-1]
                email_counter+=1

            for website in i.xpath("a[@data-original-title='Webseite']/@href").getall():
                item["website"+str(website_counter)]=str(website)
                website_counter+=1

            for facebook in i.xpath("a[@data-original-title='Facebook']/@href").getall():
                item["facebook"+str(facebook_counter)]=str(facebook)
                facebook_counter+=1

            for twitter in i.xpath("a[@data-original-title='Twitter']/@href").getall():
                item["twitter"+str(twitter_counter)]=str(twitter)
                twitter_counter+=1

            for instagram in i.xpath("a[@data-original-title='Instagram']/@href").getall():
                item["instagram"+str(instagram_counter)]=str(instagram)
                instagram_counter+=1

            for twitch in i.xpath("a[@data-original-title='Twitch']/@href").getall():
                item["twitter"+str(twitch_counter)]=str(twitch)
                twitch_counter+=1
        return item
